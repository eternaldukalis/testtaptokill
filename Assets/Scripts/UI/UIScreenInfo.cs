﻿using System;
using UnityEngine;

[Serializable]
public class UIScreenInfo
{
    public GameState State => _state;
    public UIScreen Screen => _screen;
    
    #pragma warning disable
    [SerializeField] private GameState _state;
    [SerializeField] private UIScreen _screen;
    #pragma warning restore
}
