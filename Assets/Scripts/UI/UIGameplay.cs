﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIGameplay : UIScreen
{
    #pragma warning disable
    [SerializeField] private Text _timerText, _scoreText, _pauseButtonText;
    [SerializeField] private string _pauseText = "Pause", _unpauseText = "Unpause";
    #pragma warning restore
    
    public override void Show()
    {
        base.Show();
        GameController.Instance.OnScoreChanged += OnScoreChanged;
        GameController.Instance.OnTimerChanged += OnTimerChanged;
        GameController.Instance.OnPauseStateChanged += OnPauseResume;
        OnScoreChanged();
        OnTimerChanged();
    }

    public override void Hide()
    {
        base.Hide();
        GameController.Instance.OnScoreChanged -= OnScoreChanged;
        GameController.Instance.OnTimerChanged -= OnTimerChanged;
        GameController.Instance.OnPauseStateChanged -= OnPauseResume;
    }
    
    private void OnTimerChanged()
    {
        _timerText.text = Mathf.CeilToInt(GameController.Instance.Time).ToString();
    }

    private void OnScoreChanged()
    {
        _scoreText.text = GameController.Instance.Score.ToString();
    }
    
    private void OnPauseResume()
    {
        _pauseButtonText.text = GameController.Instance.IsPlaying ? _pauseText : _unpauseText;
    }
}
