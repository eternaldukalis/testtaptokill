﻿using UnityEngine;

public class UIManager : MonoBehaviour
{
    #pragma warning disable
    [SerializeField] private UIScreenInfo[] _screens;
    #pragma warning restore

    public void StartGame()
    {
        GameManager.Instance.ChangeState(GameState.Game);
    }

    public void SwitchPause()
    {
        GameController.Instance.SwitchPause();
    }

    private void Start()
    {
        GameManager.Instance.OnStateChanged += OnStateChanged;
    }

    private void OnDestroy()
    {
        GameManager.Instance.OnStateChanged -= OnStateChanged;
    }

    private void OnStateChanged()
    {
        foreach (var x in _screens)
        {
            if (x.State == GameManager.Instance.CurrentState)
            {
                x.Screen.Show();
            }
            else
            {
                x.Screen.Hide();
            }
        }
    }
}