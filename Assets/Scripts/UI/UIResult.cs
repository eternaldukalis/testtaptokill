﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIResult : UIScreen
{
    #pragma warning disable
    [SerializeField] private Text _scoreText, _highScoreText;
    #pragma warning restore
    
    public override void Show()
    {
        base.Show();
        _scoreText.text = GameController.Instance.Score.ToString();
        _highScoreText.text = GameManager.Instance.Passport.HighScore.ToString();
    }
}
