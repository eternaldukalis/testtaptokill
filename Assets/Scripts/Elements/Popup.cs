﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Popup : Selectable
{
    public static event System.Action<int> OnPopupTapped;
    
    #pragma warning disable
    [SerializeField] private int _points;
    [SerializeField] private SpriteRenderer _renderer;
    [SerializeField] private Color _positiveColor, _negativeColor;
    #pragma warning restore

    private bool _isNegative;
    
    public void Init(bool isNegative)
    {
        _isNegative = isNegative;
        _renderer.color = isNegative ? _negativeColor : _positiveColor;
    }

    public override void OnPointerDown(PointerEventData eventData)
    {
        base.OnPointerDown(eventData);
        if (!GameController.Instance.IsPlaying)
            return;
        var points = _points * (1 - 2 * _isNegative.GetHashCode());
        Destroy(gameObject);
        OnPopupTapped?.Invoke(points);
    }
}
