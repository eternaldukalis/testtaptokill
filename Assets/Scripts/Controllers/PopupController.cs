﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

[Serializable]
public class PopupController : MonoBehaviour
{
    #pragma warning disable
    [SerializeField] private Popup[] _popupPrefabs;
    [SerializeField] private float _negativeChance = 0.2f;
    
    private List<Popup> _instantiatedPopups;
    #pragma warning restore

    public void ResetObjects()
    {
        if (_instantiatedPopups != null)
        {
            foreach (var x in _instantiatedPopups)
            {
                if (x != null)
                    Destroy(x.gameObject);
            }
        }

        _instantiatedPopups = new List<Popup>();
    }

    public void InstantiateNextPopup()
    {
        if (_popupPrefabs.Length == 0)
        {
            Debug.LogError("No popup prefabs.");
            return;
        }

        Popup popup = _popupPrefabs[Random.Range(0, _popupPrefabs.Length)];
        var inst = Instantiate(popup);
        bool negative = Random.Range(0f, 1f) <= _negativeChance;
        inst.Init(negative);
        _instantiatedPopups.Add(inst);
        float x = CameraController.Instance.Width;
        x = Random.Range(-x, x);
        float y = CameraController.Instance.Height;
        y = Random.Range(-y, y);
        inst.transform.position = new Vector3(x, y, inst.transform.position.z);
        inst.transform.SetParent(transform);
    }

    private void Start()
    {
        Popup.OnPopupTapped += OnPopupTapped;
    }

    private void OnPopupTapped(int obj)
    {
        _instantiatedPopups.RemoveAll(x => x == null);
    }
}
