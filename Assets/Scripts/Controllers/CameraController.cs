﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public static CameraController Instance => _instance;
    
    private static CameraController _instance;

    public float Height { private set; get; }
    public float Width { private set; get; }

    #pragma warning disable
    [SerializeField] private Camera _camera;
    #pragma warning restore

    private void Awake()
    {
        if (_instance != null)
        {
            Destroy(gameObject);
            return;
        }
        _instance = this;
        Height = _camera.orthographicSize;
        Width = Height * _camera.aspect;
    }
}
