﻿using System;
using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

public class GameController : MonoBehaviour
{
    public static GameController Instance { private set; get; }
    
    public event Action OnTimerChanged;
    public event Action OnScoreChanged;
    public event Action OnPauseStateChanged;

    public float Time { private set; get; }
    public int Score { private set; get; }
    public bool IsPlaying { private set; get; }

#pragma warning disable
    [SerializeField] private float _sessionTime = 60;
    [SerializeField] private float _minSpawnTime = 0.2f;
    [SerializeField] private float _maxSpawnTime = 0.5f;
    [SerializeField] private PopupController _popupController;
    #pragma warning restore
    
    private Coroutine _gameRuntime;

    public void SwitchPause()
    {
        IsPlaying = !IsPlaying;
        OnPauseStateChanged?.Invoke();
    }

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }

        Instance = this;
    }

    private void Start()
    {
        GameManager.Instance.OnStateChanged += OnStateChanged;
        Popup.OnPopupTapped += OnPopupTapped;
    }

    private void OnDestroy()
    {
        GameManager.Instance.OnStateChanged -= OnStateChanged;
        Popup.OnPopupTapped -= OnPopupTapped;
    }

    private void OnStateChanged()
    {
        if (GameManager.Instance.CurrentState == GameState.Game)
        {
            StartGame();
        }
        else
        {
            StopGame();
        }
    }

    private void StartGame()
    {
        Score = 0;
        OnScoreChanged?.Invoke();
        IsPlaying = true;
        if (_gameRuntime != null)
            StopCoroutine(_gameRuntime);
        _popupController.ResetObjects();
        _gameRuntime = StartCoroutine(GameRuntime());
    }

    private void StopGame()
    {
        if (!IsPlaying)
            return;
        IsPlaying = false;
    }
    
    private void OnPopupTapped(int obj)
    {
        Score += obj;
        OnScoreChanged?.Invoke();
    }

    private IEnumerator GameRuntime()
    {
        Time = _sessionTime;
        float timeTillPopup = 0;
        while (Time > 0)
        {
            yield return null;
            if (!IsPlaying)
                continue;
            Time -= UnityEngine.Time.deltaTime;
            OnTimerChanged?.Invoke();
            timeTillPopup -= UnityEngine.Time.deltaTime;
            if (timeTillPopup <= 0)
            {
                _popupController.InstantiateNextPopup();
                timeTillPopup = Random.Range(_minSpawnTime, _maxSpawnTime);
            }
        }

        IsPlaying = false;
        if (Score > GameManager.Instance.Passport.HighScore)
        {
            GameManager.Instance.Passport.HighScore = Score;
            GameManager.Instance.SavePassport();
        }
        GameManager.Instance.ChangeState(GameState.Result);
    }
}
