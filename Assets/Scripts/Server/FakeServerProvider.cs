﻿using System;
using System.Collections;
using UnityEngine;

public class FakeServerProvider : BaseServerProvider
{
    private const string PassportKey = "Passport";
    
    [SerializeField] private float _requestDelay = 0.5f;

    private StringSerializer _serializer = new JsonStringSerializer();
    private Coroutine _requestCoroutine;
    
    public override void GetUserPassport(Action<UserPassport> response)
    {
        MakeRequest(() =>
        {
            if (PlayerPrefs.HasKey(PassportKey))
            {
                var str = PlayerPrefs.GetString(PassportKey);
                var res = _serializer.Deserialize<UserPassport>(str);
                response?.Invoke(res);
            }
            else
            {
                response?.Invoke(new UserPassport());
            }
        });
    }

    public override void SendUserPassport(UserPassport passport, Action response)
    {
        var ser = _serializer.Serialize(passport);
        PlayerPrefs.SetString(PassportKey, ser);
        MakeRequest(response);
    }

    private void MakeRequest(Action response)
    {
        if (_requestCoroutine != null)
            StopCoroutine(_requestCoroutine);
        _requestCoroutine = StartCoroutine(RequestPlaceholder(response));
    }

    private IEnumerator RequestPlaceholder(Action response)
    {
        yield return new WaitForSeconds(_requestDelay);
        response?.Invoke();
    }
}
