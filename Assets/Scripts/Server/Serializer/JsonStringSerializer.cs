﻿using UnityEngine;

public class JsonStringSerializer : StringSerializer
{
    public override string Serialize(object obj)
    {
        return JsonUtility.ToJson(obj);
    }

    public override T Deserialize<T>(string source)
    {
        return JsonUtility.FromJson<T>(source);
    }
}
