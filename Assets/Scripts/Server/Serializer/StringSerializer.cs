﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class StringSerializer
{
    public abstract string Serialize(object obj);
    public abstract T Deserialize<T>(string source);
}
