﻿using System;
using UnityEngine;

public abstract class BaseServerProvider : MonoBehaviour
{
    public abstract void GetUserPassport(Action<UserPassport> response);

    public abstract void SendUserPassport(UserPassport passport, Action response);
}
