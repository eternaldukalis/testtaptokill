﻿using System;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance => _instance;

    private static GameManager _instance;

    public event Action OnStateChanged; 
    
    public UserPassport Passport { private set; get; }
    public GameState CurrentState { private set; get; } = GameState.None;

    #pragma warning disable
    [SerializeField] private BaseServerProvider _server;
    #pragma warning restore

    public void ChangeState(GameState state)
    {
        CurrentState = state;
        OnStateChanged?.Invoke();
    }

    public void SavePassport()
    {
        _server.SendUserPassport(Passport, null);
    }

    void Awake()
    {
        if (_instance != null)
        {
            Destroy(gameObject);
            return;
        }

        _instance = this;
        _server.GetUserPassport(obj =>
        {
            Passport = obj;
            ChangeState(GameState.MainMenu);
        });
    }
}
