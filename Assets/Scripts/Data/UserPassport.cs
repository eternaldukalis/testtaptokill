﻿using System;
using UnityEngine;

[Serializable]
public class UserPassport
{
    public int HighScore
    {
        get => _highScore;
        set => _highScore = value;
    }

    [SerializeField] private int _highScore;
}
