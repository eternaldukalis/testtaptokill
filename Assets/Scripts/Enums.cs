﻿public enum GameState
{
    None = 0,
    MainMenu = 1,
    Game = 2,
    Result = 3
}
